          <?php $token = $this->security->getToken() ?>

          <!-- Page heading -->
          <div class="page-head">
            <h2 class="pull-left"><i class="icon-table"></i> Towns</h2>

            <!-- Breadcrumb -->
            <div class="bread-crumb pull-right">
              <a href="index.html"><i class="icon-home"></i> Home</a> 
              <!-- Divider -->
              <span class="divider">/</span> 
              {{ link_to('admin/towns', 'Towns') }}
              <span class="divider">/</span> 
              <span>{{ head }}</span>
            </div>

            <div class="clearfix"></div>

          </div>
          <!-- Page heading ends -->

          <!-- Matter -->

          <div class="matter">
          {{ errNotice }}
            {{ form('class': 'form-horizontal', 'id':'main-table-form') }}
            <form name="postform" method="post" action="">
            <div class="container">

              <!-- Table -->

              <div class="row">

                <div class="col-md-12">
                  {#{ content() }#}


                  <div class="widget">

                    <div class="widget-head">
                      <div class="pull-left">{{ head }}</div>
                      <div class="widget-icons pull-right">
                      </div>  
                      <div class="clearfix"></div>
                    </div>


                    <div class="widget-content">

                      <!-- <div id="us2" style="width: 100%; height: 500px;"></div>
                      <input type="hidden" id="us2-address" class="form-control" /> -->

                        <div id="mapPanel">
                          <button onclick="addDefaultMarker();" type="button" class="btn btn-primary"><i class="icon-map-marker"></i> Add Marker</button>
                          <span> &nbsp; or click on the map to add marker.</span>
                          <button onclick="deleteMarkers();" type=button class="btn btn-warning pull-right"><span class="glyphicon glyphicon-refresh"></span> Reset Marker</button>
                        </div>
                        <div style="padding-left:1%;">
                         <em style="color:##d3d3d3; font-size:12px;">*Drag the marker to pin desired town.</em>
                        </div>
                        <div id="map-canvas" onclick="showMarkers();" style="height:500px; width 100%"></div>

                      <div class="padd">

                          <div class="form-group">
                            <div class="col-lg-2">
                              <label>Latitude</label>
                            </div>
                              <div class="col-lg-8">
                                  {% if editMode %}
                                    <?php echo $form->render('townLat', array( 'value' => $town->townLat)); ?>
                                  {% else %}
                                  {{ form.render('townLat') }}
                                  {% endif %}
                                  
                                  {{ form.messages('townLat') }}
                              </div>

                          </div>
                          <div class="form-group">
                            <div class="col-lg-2">
                              <label>Longtitude</label>
                            </div>
                              <div class="col-lg-8">

                                  {% if editMode %}
                                    <?php echo $form->render('townLong', array( 'value' => $town->townLong)); ?>
                                  {% else %}
                                  {{ form.render('townLong') }}
                                  {% endif %}
                                  
                                  {{ form.messages('townLong') }}
                              </div>
                          </div>

                          <hr />
                          
                          <div class="form-group">
                            <div class="col-lg-2">
                              <label>Town Name <apan class="asterisk">*</span></label>
                              
                            </div>
                              <div class="col-lg-8">
                                  {% if editMode %}
                                    <input type="hidden" name="origTownName" value="{{ town.townName }}">
                                    <?php echo $form->render('townName', array( 'value' => $town->townName)); ?>
                                    <div id="errTName">{{ errTName }}</div>
                                  {% else %}
                                  {{ form.render('townName') }}
                                  <div id="errTName">{{ errTName }}</div>
                                  {% endif %}
                                  {{ form.messages('townName') }}
                              </div>

                          </div>

                          <!-- <div class="form-group">
                              <label class="col-lg-4 control-label"></label>
                              <div class="col-lg-8">
                                  <a href="#partnerGallery" data-toggle="modal" class="btn btn-default pull-right post-add-media"><i class="icon-paper-clip"></i> Add Media</a>
                              </div>
                          </div> -->


                          <div class="form-group">
                             <div class="col-lg-2">
                              <label>Town Information <span class="asterisk">*</span></label>
                            </div>
                              <div class="col-lg-8">
                              
                              {% if editMode %}
                                <?php echo $form->render('townInfo', array( 'value' => $town->townInfo)); ?>
                                <div id="errdetails">{{ errTInfo }}</div>
                                <!-- {{ errTInfo }} -->
                              {% else %}
                              {{ form.render('townInfo') }}
                              {{ form.messages('townInfo') }}
                              <!-- {{ errTInfo }} -->
                              <div id="errdetails">{{ errTInfo }}</div>
                               {% endif %}
                              </div>
                          </div>

                      </div>
                      <div class="widget-foot">
                        <!-- {{ cancel }} -->
                          <!-- {{ submit_button( message , 'name':'saveTown', 'class':'btn btn-primary') }} -->
                          {% if editMode %}
                          <button type="submit" value="send" name="saveTown" id="saveTown" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-disk"></span> Update Town</button>
                           <button class="btn btn-danger"><a href="/admin/towns/" style="color: white; text-decoration: none;"><span class="glyphicon glyphicon-remove"></span> Cancel</a></button>
                           
                          {% else %}
                          <button type="submit" value="send" name="saveTown" id="saveTown" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-disk"></span> Save Town</button>
                           <a href="/admin/towns/" class="btn btn-danger" ><span class="glyphicon glyphicon-remove"></span> Cancel</a>
                           {% endif %}
                        <!--   <input type="hidden" name="csrf" value="<?php // echo $this->security->getToken() ?>"/> -->
                          <div class="clearfix"></div>
                      </div>
                    </div>

                  </div>


                </div>

              </div>


            </div>
          </form>
        </div>
<script type="text/javascript">
    $(document).ready(function(){
        //event_name
        $('#townName').on('keyup keydown keypress change paste', function() {
            if($(this).val() == ''){
                $('#errTName').html("<div class='label label-danger'>Name is required.</div>");
            }else{
                $('#errTName').html("");
            }
        });

        for (var i in CKEDITOR.instances) {
                CKEDITOR.instances[i].on('change', function() {
                    console.log(CKEDITOR.instances[i].document.getBody().getText());
                    var x = CKEDITOR.instances[i].document.getBody().getText();
                    // $('#errdetails').html("");
                    if(x == ""){
                      // $('.errdetails').html("<div class='label label-danger'>Name is required.</div>");
                      console.log('none');
                    }
                });   
        }    
      });
</script>
<!-- Matter ends -->