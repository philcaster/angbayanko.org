<!-- Page heading -->


<div class="page-head">
    <!-- Page heading -->
    <h2 class="pull-left">
        <!-- page meta -->
        <span class="page-meta">Create New Announcement</span>
    </h2>


    <!-- Breadcrumb -->
    <div class="bread-crumb pull-right">
        <a href="/admin"><i class="icon-home"></i> Home</a>
        <!-- Divider -->
        <span class="divider">/</span>
        <a href="/admin/announcements">Announcements</a>
        <!-- Divider -->
        <span class="divider">/</span>
        <a href="/admin/createannocements" class="bread-current">Create</a>
    </div>

    <div class="clearfix"></div>

</div>
<!-- Page heading ends -->
<!-- Matter -->
                
<div class="matter">

    <div class="container">
<!--        <form name="postform" method="post" action="admin/"> -->
             
            <div class="row">

                <div class="col-md-12">
                    {{ content() }}
                    {{ form('admin/createannocements') }}      
                    
                    <div class="widget">
                        <div class="widget-head">
                            <div class="pull-left">Make Announcement</div>
                            <div class="widget-icons pull-right">
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="widget-content">
                            <div class="padd">
                            
                                <div class="form-group">
                                <div class="control-label">
                                    <label>Title</label>
                                    <span class="asterisk">*</span>
                                </div>
                                    <div>
                                        {{ form.render('ann_title') }}
                                       <!--  {{ form.messages('ann_title') }} -->
                                       <span id="hidetitle"> {{ titleErr }} </span>              
                                    </div>
                                    {{ hidden_field('hpage_slug', 'id':'hpage_slug') }}
                                          <label>Slug:</label> <span id="page-slug"></span>
                            
                                </div>

                                <div class="text-area">
                                    <div class="control-label">
                                    <label>Content</label>
                                    <span class="asterisk">*</span>
                                </div>
                                 <div>
                                        {{ form.render('ann_content') }}
                                    <!--     {{ form.messages('ann_content') }} -->
                                       <span id="errdetails"> {{ contentErr }} </span>
                                    </div>

                                <br />
                                <div class="form-group">
                                 <div class="control-label">   
                                    <label>Announcement Duration</label>
                                    <span class="asterisk">*</span>
                                </div>
                                    <div class="input-append">

                                        <span>From </span>
                                        
                                        <span id="dateStart" class="start">

                                            {{ form.render('ann_start') }}
                                            <span class="add-on" id="valdate">
                                                <i class="btn btn-info btn-lg icon-calendar"></i>
                                            </span>

                                           
                                        </span>
                                    
                                        <span>To </span>
                                        <span id="dateEnd"  class="end">
                                            {{ form.render('ann_end') }}
                                            <span class="add-on" id="ed">
                                                <i class="btn btn-info btn-lg icon-calendar" id="triggerend"></i>
                                            </span>
                                        </span>
                                    </div>
                                    <div   style="margin-left: 3.5%">
                                       <table style="width: 365px">
                                        <tr>
                                            <th style="margin-right: 100%;width: 100px">
                                        <label id="valdate1">{{startErr}}</label>
                                            </th>
                                            <th  style="margin-left: 115%;width: 100px">
                                        <label id="valend">{{ endErr }}</label>
                                            </th>
                                        </tr>
                                       </table>
                                
                                    </div>
                                </div>
                            </div>
                            <div class="widget-foot">

                               <!--  {{ submit_button('Publish Announcement' ,'id':'ann_submit', 'class':'btn btn-primary pull-right') }} -->
                               <button type="submit" id="ann_submit" class="btn btn-primary pull-right">
                                <span class="glyphicon glyphicon-floppy-disk"></span> Publish Announcement</button>
                               <!--  <input type="hidden" name="csrf" value="<?php // echo $this->security->getToken() ?>"/> -->
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Matter ends -->
<!-- <script type="text/javascript">
    $('#ann_title')
    .keyup(function(){
        txtboxtitle();
    });
    function txtboxtitle(){
        $("#titlehide").hide();
    }

        $('#ann_content')
    .keyup(function(){
        txtboxcontent();
    })
    .change(function(){
        txtboxcontent();
    })
    .keydown(function(){
        txtboxcontent();
    })    
    ;
    function txtboxcontent(){
        $("#contenthide").hide();
    }

    $('#ann_start')
    .move(function(){
        textboxstartdate();
    });
    function textboxstartdate (){
        $("#startdatehide").hide();
    }

        $('#triggerend')
    .mouseclick(function(){
        textboxenddate();
    })
    .load(function(){
        textboxenddate();
    });

    function textboxenddate(){
        $("#enddatehide").hide();
    }
</script> -->
