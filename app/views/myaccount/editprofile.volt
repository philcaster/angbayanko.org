<div class="inner-programs pull-left">
	<div class="tabs inner-tabs">
	        {{ content() }}
	        <div class="panel panel-default border-flat">
				<div class="panel-heading">Edit Profile</div>
				<div class="panel-body">
					<div id="editprofileResult"></div>
			  		<form method="post" id="editprofileForm" action="/myaccount/editprofile">
			  			<div class="form-group">
			                <label>Your Title</label>
			                <div class="row">
			                  <div class="col-xs-4">
			                    <select name="title" class="form-control">
			                      <option {{ abkVolUser.title == "Mr." ? "selected='selected'" : "" }} value="Mr.">Mr.</option>
			                      <option {{ abkVolUser.title == "Ms." ? "selected='selected'" : "" }} value="Ms.">Ms.</option>
			                      <option {{ abkVolUser.title == "Mrs." ? "selected='selected'" : "" }} value="Mrs.">Mrs.</option>
			                    </select>
			                  </div>
			                </div>
			              </div>

			              <div class="form-group">
			                <label>First Name</label>
			                <input name="fname" type="text" class="form-control border-flat" value="{{ abkVolUser.fname }}">
			              </div>

			              <div class="form-group">
			                <label>Middle Name</label>
			                <input name="mname" type="text" class="form-control border-flat" value="{{ abkVolUser.mname }}">
			              </div>

			              <div class="form-group">
			                <label>Last Name</label>
			                <input name="lname" type="text" class="form-control border-flat" value="{{ abkVolUser.lname }}">
			              </div>

			              <div class="form-group">
			                <label>Extension Name</label>
			                <div class="row">
			                  <div class="col-xs-4">
			                    <input name="extname" type="text" class="form-control border-flat" value="{{ abkVolUser.extname }}" placeholder="N/A">
			                  </div>
			                </div>
			              </div>

			              <div class="form-group">
			                <label>Complete Address</label>
			                <input name="address" type="text" class="form-control border-flat" value="{{ abkVolUser.address }}">
			              </div>

			              <div class="form-group">
			                <label>Phone Number</label>
			                <input name="phone" type="text" class="form-control border-flat phone" value="{{ abkVolUser.phone }}" placeholder="N/A">
			              </div>

            			<button type="submit" class="btn btn-success" id="submitEditProfile">Update</button>
            			<a type="button" class="btn btn-default" href="/myaccount">Cancel</a>
			  		</form>
				</div>
			</div>
    </div>
</div>