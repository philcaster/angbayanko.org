<div class="inner-programs pull-left">
	<div class="tabs inner-tabs">
        {{ content() }}
        <div class="panel panel-default border-flat">
			<div class="panel-heading">Reset Password</div>
			<div class="panel-body">
				<div id="resetpassResult"></div>
		  		<form method="post" id="resetpassForm" action="/myaccount/resetpassword/{{ email }}/{{ token }}">
		            <div class="form-group">
		                <label>New Password</label>
		                <input name="password" type="password" class="form-control border-flat" placeholder="Enter new password">
		            </div>
		            <div class="form-group">
		                <label>Confirm Password</label>
		                <input name="repassword" type="password" class="form-control border-flat" placeholder="Confirm new password">
		            </div>
		            <button type="submit" class="btn btn-danger" id="submitResetpass">Reset</button>
        			<a type="button" class="btn btn-default" href="/">Cancel</a>
		  		</form>
			</div>
		</div>
	</div>
</div>