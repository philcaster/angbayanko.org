<div class="inner-programs pull-left">
	<div class="tabs inner-tabs">
	        {{ content() }}
	        <div class="panel panel-default border-flat">
			  <div class="panel-heading">My Profile</div>
			  <div class="panel-body">
			    <div class="row">
			    	<div class="col-md-12">
				    	<div class="form-group">
			                <label>Full Name</label>
			                <div class="row">
			                <div class="col-md-12">
			                <h5><strong>{{ abkVolUser.title }} {{ abkVolUser.fname }} {{ abkVolUser.mname }} {{ abkVolUser.lname }} {{ abkVolUser.extname }}</strong></h5>
			                </div>
			                </div>
			            </div>
				    </div>
			    </div>
			    <div class="row">
			    	<div class="col-md-12">
				    	<div class="form-group">
			                <label>Address</label>
			                <div class="row">
			                <div class="col-md-12">
			                <h5><strong>{{ abkVolUser.address }}</strong></h5>
			                </div>
			                </div>
			            </div>
				    </div>
			    </div>
			    <div class="row">
			    	<div class="col-md-6">
				    	<div class="form-group">
			                <label>Contact Number</label>
			                <div class="row">
			                <div class="col-md-12">
			                <h5><strong>{{ abkVolUser.phone }}</strong></h5>
			                </div>
			                </div>
			            </div>
				    </div>
				    <div class="col-md-6">
				    	<div class="form-group">
			                <label>Email Address</label>
			                <div class="row">
			                <div class="col-md-12">
			                <h5><strong>{{ abkVolUser.email }}</strong></h5>
			                </div>
			                </div>
			            </div>
				    </div>
			    </div>
			  </div>
			</div>
			{% if volunteerActivities != null %}
			<div class="panel panel-default border-flat">
			  <div class="panel-heading">Signed Up Activities</div>
			  <div class="panel-body">
			    {{ volunteerActivities }}
			  </div>
			</div>
			{% endif %}

			{% if volunteerSuggActivities != null %}
			<div class="panel panel-default border-flat">
			  <div class="panel-heading">Suggested Activities</div>
			  <div class="panel-body">
			    {{ volunteerSuggActivities }}
			  </div>
			</div>
			{% endif %}
    </div>
</div>